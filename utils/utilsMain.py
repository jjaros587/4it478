import unittest
from configs import config
from configs.suite import currentSuite
from HtmlTestRunner import HTMLTestRunner
import os
import shutil
import argparse


def parse_args():
    parse = argparse.ArgumentParser()
    parse.add_argument("-e", "--env", required=True, help="Název prostředí: dev/test/stage/prod")
    args = parse.parse_args()
    return {'parse': parse, 'args': args}


def get_env(parse, args):
    if args.env in config.environments:
        os.environ['env'] = args.env
    else:
        print("Zadejte prosím platný název prostředí")
        parse.print_help()
        quit(2)


def run_tests():
    for browser in config.browsers:
        if browser['allowed']:
            print(browser['name'])
            os.environ['BROWSER'] = browser['name']
            tests = []
            for item in currentSuite:
                tests.append(unittest.TestLoader().loadTestsFromTestCase(item))
            HTMLTestRunner(combine_reports=True,
                           report_name='TestResults_' + os.environ.get("BROWSER"),
                           add_timestamp=False,
                           report_title='Report - ' + os.environ.get("BROWSER")).run(unittest.TestSuite(tests))


def delete_folders():
    paths = ['screenshots/', 'reports/', 'output/']
    for path in paths:
        if os.path.exists(path):
            shutil.rmtree(path)


def zip_screenshots():
    path = 'screenshots/'
    if os.path.exists(path):
        shutil.make_archive('output/screenshots', 'zip', path)