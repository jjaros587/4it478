from tests.BaseTest import BaseTest
from pages.employees.PrehledZamestnancuPage import PrehledZamestnancuPage
from pages.navigation.ZamestnanciPage import ZamestnanciPage
from pages.employees.NewEmployeePage import NewEmployeePage
from pages.employees.personalInquiry.PersonalInquiry1Page import PersonalInquiry1Page
from pages.employees.personalInquiry.PersonalInquiry2Page import PersonalInquiry2Page
from pages.employees.personalInquiry.PersonalInquiry3Page import PersonalInquiry3Page
from pages.login.LoginPage import LoginPage
from ddt import ddt, file_data
from configs import config
from utils.utilsTest import get_login_credentials
from configs.locators import OtherLocators


@ddt
class PersonalInquiryTest(BaseTest):
    path = config.dataPath + 'employeeManagement/personalInquiry/'

    @file_data(path + "personalInquiry_positive.json")
    def test_personal_inquiry_positive(self, employee, inquiry1, inquiry2, inquiry3):
        LoginPage(self.driver).login(*get_login_credentials())
        ZamestnanciPage(self.driver).prehled_zamestnancu_click()
        PrehledZamestnancuPage(self.driver).click_buttonNovyZamestnanec()
        NewEmployeePage(self.driver).fill_form_basic_positive(self.email, *employee.values())

        self.driver.find_element_by_link_text("kartu zaměstnance").click()
        self.driver.find_element_by_id("btn-fill-inquiry").click()

        self.assertTrue('contract-info' in self.driver.current_url)
        PersonalInquiry1Page(self.driver).fill_form_part1_positive(*inquiry1.values())
        self.assertTrue('other-employee-details' in self.driver.current_url)
        PersonalInquiry2Page(self.driver).fill_form_part2_positive(*inquiry2.values())
        self.assertTrue('wages-info' in self.driver.current_url)
        PersonalInquiry3Page(self.driver).fill_form_part3_positive(*inquiry3.values())

        self.assertTrue('summary' in self.driver.current_url)
        self.driver.find_element(*OtherLocators.button_schvalitDotaznik).click()
        self.assertTrue('employee-card' in self.driver.current_url)
