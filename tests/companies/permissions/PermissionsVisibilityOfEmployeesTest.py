from pages.companies.newCompany.NewCompanyMasterPage import NewCompanyMasterPage
from pages.employees.employments.newEmployment.NewEmploymentMasterPage import NewEmploymentMasterPage
from pages.SearchPage import SearchPage
from tests.BaseTest import BaseTest
from configs import config
from utils import utilsTest
import json
from pages.login.LoginPage import LoginPage
from utils.utilsTest import get_login_credentials
from selenium.webdriver.support.events import EventFiringWebDriver
from utils.ScreenshotListener import ScreenshotListener
from utils.utilsTest import element_exists
from configs.locators import SearchPageLocators


class PermissionsUnitsTest(BaseTest):
    path = config.dataPath + 'companies\\permissions\\'

    emailSuperior = utilsTest.create_email()
    emailSubordinate = utilsTest.create_email()
    emailOther = utilsTest.create_email()
    emailOtherSuperior = utilsTest.create_email()
    allEmails = [emailSuperior, emailSubordinate, emailOther, emailOtherSuperior]
    data = None
    assertionError = None

    @classmethod
    def setUpClass(cls):
        super().run_driver(cls)
        LoginPage(cls.driver).test_basic_positive(*get_login_credentials())
        cls.create_data()
        cls.driver.quit()

    def setUp(self):
        if self.assertionError != None:
            self.fail(self.assertionError)
        super().run_driver()
        self.driver = EventFiringWebDriver(self.driver, ScreenshotListener(self._testMethodName))

    def tearDown(self):
        self.driver.quit()

    @classmethod
    def tearDownClass(cls):
        super(PermissionsUnitsTest, cls).run_driver(cls)
        LoginPage(cls.driver).test_basic_positive(*get_login_credentials())
        cls.delete_all_users()
        super(PermissionsUnitsTest, cls).delete_company(cls)
        cls.driver.quit()

    # TESTS -----------------------------------------------------------------------------------------------------------------------
    def test_superior(self):
        LoginPage(self.driver).login_as_user(self.emailSuperior)
        error = "Nadřízený %s nevidí svého podřízeného %s!"
        self.verify_user(True, self.emailSuperior, self.data['subordinate']['inquiry']['part1']['birth_number'], error)
        error = "Nadřízený %s vidí zaměstnance %s, jehož není nadřízeným!"
        self.verify_user(False, self.emailSuperior, self.data['other']['inquiry']['part1']['birth_number'], error)
        error = "Nadřízený %s vidí zaměstnance %s, jehož není nadřízeným! Tento zaměstnanec je nadřízeným své pozice!"
        self.verify_user(False, self.emailSuperior, self.data['otherSuperior']['inquiry']['part1']['birth_number'], error)

    def test_subordinate(self):
        LoginPage(self.driver).login_as_user(self.emailOther)
        if element_exists(self.driver, SearchPageLocators.button_show_search_form):
            self.fail("Zaměstnanci je přístupno vyhledávání i když není ničím nadřízeným!")

    def test_other_superior(self):
        LoginPage(self.driver).login_as_user(self.emailOtherSuperior)
        error = "Zaměstnanec %s vidí jiného zaměstnance %s! Tento zaměstnanec není ničím nadřízeným a je podřízeným jiného zaměstnance!"
        self.verify_user(False, self.emailOtherSuperior, self.data['subordinate']['inquiry']['part1']['birth_number'], error)
        error = "Zaměstnanec %s vidí jiného zaměstnance %s! Tento zaměstnanec není níčím nadřízeným ani podřízeným!"
        self.verify_user(False, self.emailOtherSuperior, self.data['other']['inquiry']['part1']['birth_number'], error)
        error = "Zaměstnanec %s vidí jiného zaměstnance %s! Tento Zaměstnanec je něčím nadřízeným!"
        self.verify_user(False, self.emailOtherSuperior, self.data['superior']['inquiry']['part1']['birth_number'], error)

    def test_other(self):
        LoginPage(self.driver).login_as_user(self.emailOther)
        if element_exists(self.driver, SearchPageLocators.button_show_search_form):
            self.fail("Zaměstnanci je přístupno vyhledávání i když není ničím nadřízeným!")

    def test_superior_can_create_subordinate_position(self):
        LoginPage(self.driver).login_as_user(self.emailSuperior)


    # OTHER METHODS ---------------------------------------------------------------------------------------------------------------------
    @classmethod
    def create_data(cls):
        try:
            with open(cls.path + 'permissions_visibility__of_employees.json') as file:
                data = json.load(file)
                cls.data = data[0]
                file.close()
            cls.ico = cls.data['company']['part1']['identification_code']
            NewCompanyMasterPage(cls.driver).test_basic_positive(False, cls.data['company'])
            NewEmploymentMasterPage(cls.driver).add_employment_without_company(cls.emailSuperior, cls.data['superior']['employee'], cls.data['superior']['inquiry'], cls.data['superior']['employment'])
            NewEmploymentMasterPage(cls.driver).add_employment_without_company(cls.emailSubordinate, cls.data['subordinate']['employee'], cls.data['subordinate']['inquiry'], cls.data['subordinate']['employment'])
            NewEmploymentMasterPage(cls.driver).add_employment_without_company(cls.emailOther, cls.data['other']['employee'], cls.data['other']['inquiry'], cls.data['other']['employment'])
            NewEmploymentMasterPage(cls.driver).add_employment_without_company(cls.emailOtherSuperior, cls.data['otherSuperior']['employee'], cls.data['otherSuperior']['inquiry'], cls.data['otherSuperior']['employment'])
        except Exception as e:
            cls.assertionError = "SELHALO VYTVOŘENÍ DAT:\n\n" + str(e)

    def verify_user(self, should_see, email, test_rc, error):
        SearchPage(self.driver).find_user_basic(test_rc)
        if should_see:
            assert "employee-card" in self.driver.current_url, error % (email, test_rc)
        else:
            assert "employee-card" not in self.driver.current_url, error % (email, test_rc)

    @classmethod
    def delete_all_users(cls):
        for email in cls.allEmails:
            cls.email = email
            super(PermissionsUnitsTest, cls).delete_user(cls)

