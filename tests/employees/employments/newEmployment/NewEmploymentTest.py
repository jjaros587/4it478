from tests.BaseTest import BaseTest
from pages.employees.employments.newEmployment.NewEmploymentMasterPage import NewEmploymentMasterPage
from ddt import ddt, file_data
from configs import config


@ddt
class NewEmploymentTest(BaseTest):
    path = config.dataPath + 'employees\\employments\\newEmployment\\'

    @file_data(path + "new_employment_positive.json")
    def test_add_employment_positive(self, company, employee, inquiry, employment):
        self.ico = company['part1']['identification_code']
        NewEmploymentMasterPage(self.driver).test_basic_positive(company, self.email, employee, inquiry, employment)

    def tearDown(self):
        super().delete_user()
        super().delete_company()
        super().check_delete_errors()
        self.driver.quit()










