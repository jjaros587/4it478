from configs.locators import SystemPageLocators
from selenium.webdriver.support.ui import WebDriverWait
from utils.AttributeHasValue import AttributeHasValue
from selenium.webdriver.common.by import By
from utils.utilsTest import waiter


class SystemPage:
    def __init__(self, driver):
        self.driver = driver
        self.system = self.driver.find_element(*SystemPageLocators.main)
        self.uzivatele = self.system.find_element(*SystemPageLocators.uzivatele)
        self.role = self.system.find_element(*SystemPageLocators.role)
        self.spolecnosti = self.system.find_element(*SystemPageLocators.spolecnosti)
        self.nastaveniAplikace = self.system.find_element(*SystemPageLocators.nastaveniAplikace)
        self.wait = WebDriverWait(self.driver, 60)

    def __call__(self):
        self.__init__(self.driver)

    def uzivatele_click(self):
        self.system.click()
        waiter(self.driver, SystemPageLocators.main, 2000, 'class', 'is-open')
        self.uzivatele.click()

    def role_click(self):
        self.system.click()
        waiter(self.driver, SystemPageLocators.main, 2000, 'class', 'is-open')
        self.role.click()

    def spolecnosti_click(self):
        self.system.click()
        waiter(self.driver, SystemPageLocators.main, 2000, 'class', 'is-open')
        self.spolecnosti.click()

    def nastaveni_aplikace_click(self):
        self.system.click()
        waiter(self.driver, SystemPageLocators.main, 2000, 'class', 'is-open')
        self.nastaveniAplikace.click()
