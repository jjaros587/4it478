from configs.locators import LoginPageLocators


class LoginPage:
    def __init__(self, driver):
        self.driver = driver
        self.username = self.driver.find_element(*LoginPageLocators.field_username)
        self.password = self.driver.find_element(*LoginPageLocators.field_password)
        self.loginButton = self.driver.find_element(*LoginPageLocators.button_login)

    def __call__(self):
        self.__init__(self.driver)

    def set_username(self, username):
        self.username.send_keys(username)

    def set_password(self, password):
        self.password.send_keys(password)

    def click_button(self):
        self.loginButton.click()

    def login(self, username, password):
        self.username.send_keys(username)
        self.password.send_keys(password)
        self.loginButton.click()