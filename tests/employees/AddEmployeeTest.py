from tests.BaseTest import BaseTest
from pages.employees.PrehledZamestnancuPage import PrehledZamestnancuPage
from pages.navigation.ZamestnanciPage import ZamestnanciPage
from pages.employees.NewEmployeePage import NewEmployeePage
from pages.login.LoginPage import LoginPage
from ddt import ddt, file_data
from configs import config
from utils.utilsTest import get_login_credentials


@ddt
class AddEmployeeTest(BaseTest):
    path = config.dataPath + 'employeeManagement\\employees\\'

    @file_data(path + "newEmployee_positive.json")
    def test_add_employee_positive(self, employee):
        LoginPage(self.driver).login(*get_login_credentials())
        ZamestnanciPage(self.driver).prehled_zamestnancu_click()
        PrehledZamestnancuPage(self.driver).click_buttonNovyZamestnanec()
        self.assertTrue('new-employee' in self.driver.current_url)
        NewEmployeePage(self.driver).fill_form_basic_positive(self.email, *employee.values())
        self.assertTrue('employee-created' in self.driver.current_url)
