from tests.BaseTest import BaseTest
from pages.employees.personalInquiry.PersonalInquiryMasterPage import PersonalInquiryMasterPage
from ddt import ddt, file_data
from configs import config


@ddt
class PersonalInquiryTest(BaseTest):
    path = config.dataPath + 'employees\\personalInquiry\\'

    @file_data(path + "personal_inquiry_positive.json")
    def test_basic_positive(self, employee, inquiry):
        PersonalInquiryMasterPage(self.driver).test_basic_positive(self.email, employee, inquiry)

    @file_data(path + "personal_inquiry_negative_empty_fields.json")
    def test_negative_empty_fields_part1(self, employee, inquiry, error):
        PersonalInquiryMasterPage(self.driver).test_negative_empty_fields_part1(self.email, employee, inquiry, error)


