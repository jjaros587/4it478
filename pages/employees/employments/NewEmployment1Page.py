from configs.locators import NewEmployment1PageLocators
from utils.utilsTest import select_dropdown_item
from utils.utilsTest import click_radio_by_value
from selenium.webdriver.common.keys import Keys
import time


class NewEmployment1Page:
    def __init__(self, driver):
        self.driver = driver
        self.radio_typ = self.driver.find_elements(*NewEmployment1PageLocators.radio_typ)
        self.field_zacatek = self.driver.find_element(*NewEmployment1PageLocators.field_zacatek)
        self.filed_prvniSmena = self.driver.find_element(*NewEmployment1PageLocators.filed_prvniSmena)
        self.dropdown_utvar = self.driver.find_element(*NewEmployment1PageLocators.dropdown_utvar)
        self.dropdown_stredisko = self.driver.find_element(*NewEmployment1PageLocators.dropdown_stredisko)
        self.dropdown_pozice = self.driver.find_element(*NewEmployment1PageLocators.dropdown_pozice)
        self.button_pokracovat = self.driver.find_element(*NewEmployment1PageLocators.button_pokracovat)

    def __call__(self):
        self.__init__(self.driver)

    def select_radio_typ(self, typ):
        click_radio_by_value(self.radio_typ, typ)

    def fill_field_zacetek(self, zacatek):
        self.field_zacatek.clear()
        self.field_zacatek.send_keys(zacatek)
        self.field_zacatek.send_keys(Keys.ENTER)

    def fill_filed_prvniSmena(self, prvniSmena):
        self.filed_prvniSmena.clear()
        self.filed_prvniSmena.send_keys(prvniSmena)
        self.filed_prvniSmena.send_keys(Keys.ENTER)

    def fill_dropdown_utvar(self, utvar):
        select_dropdown_item(self.driver, self.dropdown_utvar, utvar)

    def fill_dropdown_stredisko(self, stredisko):
        select_dropdown_item(self.driver, self.dropdown_stredisko, stredisko)

    def fill_dropdown_pozice(self, pozice):
        time.sleep(0.2)
        select_dropdown_item(self.driver, self.dropdown_pozice, pozice)

    def fill_dropdown_nadrizeny(self, nadrizeny):
        time.sleep(0.2)
        element = self.driver.find_element(*NewEmployment1PageLocators.dropdown_nadrizeny)
        select_dropdown_item(self.driver, element, nadrizeny)

    def fill_dropdown_schvalovatel(self, schvalovatel):
        element = self.driver.find_element(*NewEmployment1PageLocators.dropdown_schvalovatel)
        select_dropdown_item(self.driver, element, schvalovatel)

    def click_button_pokracovat(self):
        self.button_pokracovat.click()

    def fill_form_part1_basic_positive(self, typ, zacatek, prvniSmena, utvar, stredisko, pozice, nadrizeny, schvalovatel):
        self.select_radio_typ(typ)
        self.fill_field_zacetek(zacatek)
        self.fill_filed_prvniSmena(prvniSmena)
        self.fill_dropdown_utvar(utvar)
        self.fill_dropdown_stredisko(stredisko)
        self.fill_dropdown_pozice(pozice)
        self.fill_dropdown_nadrizeny(nadrizeny)
        self.fill_dropdown_schvalovatel(schvalovatel)
        self.click_button_pokracovat()

