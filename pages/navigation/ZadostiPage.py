from configs.locators import ZadostiPageLocators


class ZadostiPage:
    def __init__(self, driver):
        self.driver = driver
        self.zadosti = self.driver.find_element(*ZadostiPageLocators.main)

    def __call__(self):
        self.__init__(self.driver)

    def zadosti_click(self):
        self.zadosti.click()
