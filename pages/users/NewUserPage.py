from configs.locators import NewUserPageLocators
from utils.utilsTest import click_checkbox_by_value
<<<<<<< HEAD
from utils.utilsTest import element_exists_with_id
=======
>>>>>>> d5040dcc32f19ea6d6db31461314147378cb0c40


class NewUserPage:
    def __init__(self, driver):
        self.driver = driver
<<<<<<< HEAD

    def __call__(self):
        self.__init__(self.driver)
        self.field_name = self.driver.find_element(*NewUserPageLocators.field_name)
        self.field_surname = self.driver.find_element(*NewUserPageLocators.field_surname)
        self.field_email = self.driver.find_element(*NewUserPageLocators.field_email)
        self.field_phone = self.driver.find_element(*NewUserPageLocators.field_phone)
        self.checkbox_user_roles = self.driver.find_elements(*NewUserPageLocators.checkbox_user_roles)
        self.checkbox_agreement = self.driver.find_element(*NewUserPageLocators.checkbox_agreement)
        self.button_save = self.driver.find_element(*NewUserPageLocators.button_save)

    def fill_field_jmeno(self, name):
        self.field_name.clear()
        self.field_name.send_keys(name)

    def fill_field_prijmeni(self, surname):
        self.field_surname.clear()
        self.field_surname.send_keys(surname)
=======
        self.field_jmeno = self.driver.find_element(*NewUserPageLocators.field_jmeno)
        self.field_prijmeni = self.driver.find_element(*NewUserPageLocators.field_prijmeni)
        self.field_email = self.driver.find_element(*NewUserPageLocators.field_email)
        self.field_telefon = self.driver.find_element(*NewUserPageLocators.field_telefon)
        self.checkbox_role = self.driver.find_elements(*NewUserPageLocators.checkbox_role)
        self.checkbox_souhlas = self.driver.find_element(*NewUserPageLocators.checkbox_souhlas)
        self.button_zalozit = self.driver.find_element(*NewUserPageLocators.button_zalozit)

    def __call__(self):
        self.__init__(self.driver)

    def fill_field_jmeno(self, jmeno):
        self.field_jmeno.clear()
        self.field_jmeno.send_keys(jmeno)

    def fill_field_prijmeni(self, prijmeni):
        self.field_prijmeni.clear()
        self.field_prijmeni.send_keys(prijmeni)
>>>>>>> d5040dcc32f19ea6d6db31461314147378cb0c40

    def fill_field_email(self, email):
        self.field_email.clear()
        self.field_email.send_keys(email)

<<<<<<< HEAD
    def fill_field_telefon(self, phone):
        self.field_phone.clear()
        self.field_phone.send_keys(phone)

    def click_checkbox_role(self, role):
        click_checkbox_by_value(self.checkbox_user_roles, role)

    def click_checkbox_souhlas(self):
        self.checkbox_agreement.click()

    def click_button_zalozit(self):
        self.button_save.click()

    def fill_form(self, email, user):
        self.driver.find_element_by_link_text("Přidat uživatele").click()
        self.__call__()
        self.fill_field_jmeno(user['name'])
        self.fill_field_prijmeni(user['surname'])
        self.fill_field_email(email)
        self.fill_field_telefon(user['phone'])
        self.click_checkbox_role(user['role'])
        self.click_checkbox_souhlas()
        self.click_button_zalozit()

    def test_basic_positive(self, email, user):
        self.fill_form(email, user)
        assert 'user-created' in self.driver.current_url, "Nepovedlo se vytvořit systémového uživatele!"

    def test_negative_empty_fields(self, email, user, error):
        self.fill_form(email, user)
        assert 'user-created' not in self.driver.current_url, "Formulář pro vytvoření uživatele se povedlo odeslat s nevalidními hodnotami!"
        assert element_exists_with_id(self.driver, error), "Nebyla zobrazena chybová hláška '%s'!" % error
=======
    def fill_field_telefon(self, telefon):
        self.field_telefon.clear()
        self.field_telefon.send_keys(telefon)

    def click_checkbox_role(self, role):
        click_checkbox_by_value(self.checkbox_role, role)

    def click_checkbox_souhlas(self):
        self.checkbox_souhlas.click()

    def click_button_zalozit(self):
        self.button_zalozit.click()

    def fill_form_basic_positive(self, email, jmeno, prijmeni, telefon, role):
        self.fill_field_jmeno(jmeno)
        self.fill_field_prijmeni(prijmeni)
        self.fill_field_email(email)
        self.fill_field_telefon(telefon)
        self.click_checkbox_role(role)
        self.click_checkbox_souhlas()
        self.click_button_zalozit()
>>>>>>> d5040dcc32f19ea6d6db31461314147378cb0c40
