from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
import json
from configs.config import dataPath
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import time
import os
import datetime
import random
import string


def element_exists(driver, locator):
    try:
        driver.find_element(*locator)
    except NoSuchElementException:
        return False
    else:
        return True


def get_login_credentials():
    with open(dataPath + 'login.json') as file:
        data = json.load(file)
    return data.values()


def click_radio_by_value(elements, value):
    for radio in elements:
        if str(value) in radio.get_attribute('value'):
            radio.find_element_by_xpath('..').click()
            return True
    raise NoSuchElementException


def click_checkbox_by_value(elements, value):
    for checkbox in elements:
        if str(value) in checkbox.get_attribute('value'):
            if not checkbox.is_selected():
                checkbox.find_element_by_xpath('../..').click()
            return True
    raise NoSuchElementException


def click_checkbox_by_value_multiple(elements, items):
    for item in items:
        for element in elements:
            if item in element.get_attribute('value'):
                element.find_element_by_xpath('..').click()
                break
        else:
            raise NoSuchElementException


def select_dropdown_item(driver, dropdown, value):
    if os.environ['BROWSER'] == "chrome":
        selector = (By.XPATH, "//option[contains(text(),'" + value + "')]")
        option_value = dropdown.find_element(*selector).get_attribute('value')
        Select(dropdown).select_by_value(option_value)

    elif os.environ['BROWSER'] == "firefox":
        dropdown.find_element_by_xpath("..").click()
        time.sleep(2)
        dropdown_input = driver.find_element(By.XPATH, "//*[contains(@class,'select2-container--open')]//*[contains(@class,'select2-search__field')]")
        dropdown_input.send_keys(value)
        dropdown_input.send_keys(Keys.ENTER)


def select_dropdown_item_known_value(driver, dropdown, value):
    if os.environ['BROWSER'] == "chrome":
        Select(dropdown).select_by_value(value)

    elif os.environ['BROWSER'] == "firefox":
        dropdown.find_element_by_xpath("..").click()
        time.sleep(2)
        dropdown_input = driver.find_element(By.XPATH, "//*[contains(@class,'select2-container--open')]//*[contains(@class,'select2-search__field')]")
        dropdown_input.send_keys(value)
        dropdown_input.send_keys(Keys.ENTER)


def element_exists_with_id(driver, elementID):
    try:
        driver.find_element_by_id(elementID)
    except NoSuchElementException:
        return False
    else:
        return True


def scroll_to_top(driver):
    driver.find_element_by_tag_name('body').send_keys(Keys.HOME)


def create_email():
    random_string = ''.join(random.sample(string.ascii_lowercase, 4))
    return datetime.datetime.today().strftime('%Y%m%d_%H%M%S') + "_" + random_string + '.test@cognevo.eu'
