from tests.BaseTest import BaseTest
<<<<<<< HEAD
from pages.users.NewUserPage import NewUserPage
from configs import config
from ddt import ddt, file_data
=======
from pages.login.LoginPage import LoginPage
from pages.users.NewUserPage import NewUserPage
from configs import config
from ddt import ddt, file_data
from utils.utilsTest import get_login_credentials
>>>>>>> d5040dcc32f19ea6d6db31461314147378cb0c40


@ddt
class NewUserTest(BaseTest):
    path = config.dataPath + "users/"

<<<<<<< HEAD
    @file_data(path + "new_user_positive.json")
    def test_positive(self, user):
        NewUserPage(self.driver).test_basic_positive(self.email, user)

    @file_data(path + "new_user_negative_empty_fields.json")
    def test_negative_empty_fields(self, user, error):
        NewUserPage(self.driver).test_negative_empty_fields(self.email, user, error)
=======
    @file_data(path + "newUser_positive.json")
    def test_new_user_positive(self, user):
        login_page = LoginPage(self.driver)
        login_page.login(*get_login_credentials())
        self.assertTrue('users' in self.driver.current_url)
        self.driver.find_element_by_link_text("Přidat uživatele").click()
        NewUserPage(self.driver).fill_form_basic_positive(self.email, *user.values())
        self.assertTrue('user-created' in self.driver.current_url)

    def tearDown(self):
        self.driver.quit()
>>>>>>> d5040dcc32f19ea6d6db31461314147378cb0c40
