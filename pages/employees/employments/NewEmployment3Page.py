from configs.locators import NewEmployment3PageLocators


class NewEmployment3Page:
    def __init__(self, driver):
        self.driver = driver
        self.button_pokracovat = self.driver.find_element(*NewEmployment3PageLocators.button_pokracovat)

    def __call__(self):
        self.__init__(self.driver)

    def click_button_pokracovat(self):
        self.button_pokracovat.click()

    def fill_form_part3_basic_positive(self):
        self.click_button_pokracovat()

