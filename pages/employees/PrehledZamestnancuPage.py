from configs.locators import PrehledZamestnancuPageLocators


class PrehledZamestnancuPage:
    def __init__(self, driver):
        self.driver = driver
        self.buttonNovyZamestnanec = self.driver.find_element(*PrehledZamestnancuPageLocators.buttonNovyZamestnanec)

    def __call__(self):
        self.__init__(self.driver)

    def click_buttonNovyZamestnanec(self):
        self.buttonNovyZamestnanec.click()