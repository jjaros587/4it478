from tests.BaseTest import BaseTest
from pages.employees.employments.extendEmployment.ExtendEmploymentMasterPage import ExtendEmploymentMasterPage
from ddt import ddt, file_data
from configs import config


@ddt
class ExtendEmploymentTest(BaseTest):
    path = config.dataPath + 'employees\\employments\\extendEmployment\\'

    @file_data(path + "extend_employment_positive.json")
    def test_add_employment_positive(self, employee, inquiry, employment, extend_employment):
        ExtendEmploymentMasterPage(self.driver).fill_form(self.email, employee, inquiry, employment, extend_employment)









