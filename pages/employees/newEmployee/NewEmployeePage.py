from configs.locators import NewEmployeePageLoators
from utils.utilsTest import select_dropdown_item
from utils.utilsTest import element_exists_with_id
from pages.navigation.MenuEmployeesPage import MenuEmployeesPage
from pages.employees.EmployeesPage import EmployeesPage


class NewEmployeePage:
    def __init__(self, driver):
        self.driver = driver

        '''
        self.dropdown_jazyk = self.driver.find_element(*NewEmployeePageLoators.dropdown_jazyk)
        self.dropdown_útvar = self.driver.find_element(*NewEmployeePageLoators.dropdown_útvar)
        '''

    def __call__(self):
        self.__init__(self.driver)
        self.field_name = self.driver.find_element(*NewEmployeePageLoators.field_name)
        self.field_surname = self.driver.find_element(*NewEmployeePageLoators.field_surname)
        self.field_email = self.driver.find_element(*NewEmployeePageLoators.field_email)
        self.field_phone = self.driver.find_element(*NewEmployeePageLoators.field_phone)

        self.dropdown_default_approver_id = self.driver.find_element(*NewEmployeePageLoators.dropdown_default_approver_id)
        self.checkbox_agreement = self.driver.find_element(*NewEmployeePageLoators.checkbox_agreement)
        self.button_save = self.driver.find_element(*NewEmployeePageLoators.button_save)

    def fill_field_name(self, name):
        self.field_name.clear()
        self.field_name.send_keys(name)

    def fill_field_surname(self, surname):
        self.field_surname.clear()
        self.field_surname.send_keys(surname)

    def fill_field_email(self, email):
        self.field_email.clear()
        self.field_email.send_keys(email)

    def fill_field_phone(self, phone):
        self.field_phone.clear()
        self.field_phone.send_keys(phone)

    def fill_dropdown_default_approver_id(self, default_approver_id):
        select_dropdown_item(self.driver, self.dropdown_default_approver_id, default_approver_id)

    def click_checkbox_agreement(self):
        self.checkbox_agreement.click()

    def click_button_save(self):
        self.button_save.click()

    def fill_form(self, email, employee):
        MenuEmployeesPage(self.driver).click_link_employees()
        EmployeesPage(self.driver).click_button_new_employee()
        self()
        self.fill_field_name(employee['name'])
        self.fill_field_surname(employee['surname'])
        self.fill_field_email(email)
        self.fill_field_phone(employee['phone'])
        self.fill_dropdown_default_approver_id(employee['default_approver_id'])
        self.click_checkbox_agreement()
        self.click_button_save()

    def test_basic_positive(self, email, employee):
        self.fill_form(email, employee)
        assert 'employee-created' in self.driver.current_url, "Zaměstnance se nepovedlo vytvořit!"

    def test_basic_negative(self, email, employee, error):
        self.fill_form(email, employee)
        assert 'employee-created' not in self.driver.current_url, "Formulář se podařilo odeslat s nevalidními údaji!"
        assert element_exists_with_id(self.driver, error), "Nebyla zobrazena chybová hláška '%s'!" % error


