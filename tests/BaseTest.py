import unittest
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
import os
from configs import config
from selenium.webdriver.support.events import EventFiringWebDriver
from utils.ScreenshotListener import ScreenshotListener
from pages.login.LoginPage import LoginPage
from utils.utilsTest import get_login_credentials
from utils import utilsTest
from pages.navigation.MenuSystemPage import MenuSystemPage
from pages.companies.CompaniesPage import CompaniesPage
from pages.SearchPage import SearchPage


class BaseTest(unittest.TestCase):
    driver = None
    email = None
    deleteFails = []
    ico = None

    def setUp(self):
        self.run_driver()
        self.driver = EventFiringWebDriver(self.driver, ScreenshotListener(self._testMethodName))
        LoginPage(self.driver).login(*get_login_credentials())
        self.email = utilsTest.create_email()

    def tearDown(self):
        self.delete_user()
        self.check_delete_errors()
        self.driver.quit()

    def run_driver(self):
        if os.environ.get('BROWSER') == "chrome":
            self.driver = webdriver.Chrome(ChromeDriverManager().install())
        elif os.environ.get('BROWSER') == "firefox":
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        self.driver.implicitly_wait(3)
        self.driver.maximize_window()
        self.driver.get(config.environments[os.environ.get('env')])

    def delete_company(self):
        MenuSystemPage(self.driver).click_link_companies()
        if not CompaniesPage(self.driver).delete_company(self.ico):
            self.deleteFails.append("Společnost s ičo '" + self.ico + "' se nepovedlo smazat z důvodu: . Je třeba ji smazat ručně!")

    def delete_user(self):
        if not SearchPage(self.driver).delete_user(self.email):
            self.deleteFails.append("Uživatele s emailem " + self.email + " se nepodařilo smazat!")
        else:
            self.driver.back()

    def check_delete_errors(self):
        if len(self.deleteFails) > 0:
            message = ""
            for fail in self.deleteFails:
                message += fail + "\n"
            self.fail(message)
