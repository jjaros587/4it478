from configs.locators import MenuEmployeesPageLocators
from utils.Waiter import Waiter


class MenuEmployeesPage:
    def __init__(self, driver):
        self.driver = driver
        self.main = self.driver.find_element(*MenuEmployeesPageLocators.main)
        self.link_employees = self.main.find_element(*MenuEmployeesPageLocators.link_employees)
        self.link_document_definitions = self.main.find_element(*MenuEmployeesPageLocators.link_document_definitions)
        self.link_changes = self.main.find_element(*MenuEmployeesPageLocators.link_changes)
        self.link_changes_new = self.main.find_element(*MenuEmployeesPageLocators.link_changes_new)
        self.link_documents_overview = self.main.find_element(*MenuEmployeesPageLocators.link_documents_overview)
        self.waiter = Waiter(self.driver, MenuEmployeesPageLocators.main)

    def __call__(self):
        self.__init__(self.driver)
        return self

    def click_link_employees(self):
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_employees.click()

    def click_link_document_definitions(self):
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_document_definitions.click()

    def click_link_changes(self):
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_changes.click()

    def click_link_changes_new(self):
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_changes_new.click()

    def click_link_documents_overview(self):
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_documents_overview.click()
