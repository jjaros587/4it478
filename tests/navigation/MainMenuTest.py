from tests.BaseTest import BaseTest
<<<<<<< HEAD
from pages.navigation.MenuEmployeesPage import MenuEmployeesPage
from pages.navigation.MenuAttendancePage import MenuAttendancePage
from pages.navigation.MenuRequestsPage import MenuRequestsPage
from pages.navigation.MenuSystemPage import MenuSystemPage
from ddt import ddt


@ddt
class MainMenuTest(BaseTest):

    def test_menu(self):
        self.employees()
        self.attendance()
        self.requests()
        self.system()

    def employees(self):
        employees = MenuEmployeesPage(self.driver)
        employees.click_link_employees()
        self.assertTrue('employees' in self.driver.current_url)
        employees().click_link_document_definitions()
        self.assertTrue('employees/document-definitions' in self.driver.current_url)
        employees().click_link_changes()
        self.assertTrue('employees/not-checked-changes' in self.driver.current_url)
        employees().click_link_changes_new()
        self.assertTrue('employees/changes/overview' in self.driver.current_url)
        employees().click_link_documents_overview()
        self.assertTrue('employees/documents-overview/accountant' in self.driver.current_url)

    def attendance(self):
        attendance = MenuAttendancePage(self.driver)
        attendance.click_link_plan()
        self.assertTrue('attendance/plan' in self.driver.current_url)
        attendance().click_link_recapitulation()
        self.assertTrue('attendance/recapitulation' in self.driver.current_url)

    def requests(self):
        MenuRequestsPage(self.driver).click_approval_requests()
        self.assertTrue('approval-requests/handled-by-me' in self.driver.current_url)

    def system(self):
        system = MenuSystemPage(self.driver)
        system.click_link_users()
        self.assertTrue('users' in self.driver.current_url)
        system().click_link_roles()
        self.assertTrue('roles' in self.driver.current_url)
        system().click_link_companies()
        self.assertTrue('companies' in self.driver.current_url)
        system().click_link_system_settings()
        self.assertTrue('system-settings' in self.driver.current_url)
        system().click_link_licence()
        self.assertTrue('licence' in self.driver.current_url)
=======
from pages.navigation.ZamestnanciPage import ZamestnanciPage
from pages.navigation.DochazkaPage import DochazkaPage
from pages.navigation.ZadostiPage import ZadostiPage
from pages.navigation.SystemPage import SystemPage
from pages.login.LoginPage import LoginPage
from ddt import ddt
from configs import config
from utils.utilsTest import get_login_credentials

@ddt
class MainMenuTest(BaseTest):
    path = config.dataPath

    def test_menu(self):
        login_page = LoginPage(self.driver)
        login_page.login(*get_login_credentials())
        self.zamestnanci()
        self.dochazka()
        self.zadosti()
        self.system()

    def zamestnanci(self):
        zamestnanci_page = ZamestnanciPage(self.driver)
        zamestnanci_page.prehled_zamestnancu_click()
        self.assertTrue('employees' in self.driver.current_url)
        zamestnanci_page()
        zamestnanci_page.nastaveni_dokumentu_click()
        self.assertTrue('employees/document-definitions' in self.driver.current_url)
        zamestnanci_page()
        zamestnanci_page.prehled_zmen_click()
        self.assertTrue('employees/changes/overview' in self.driver.current_url)
        zamestnanci_page()
        zamestnanci_page.nahrane_dokumenty_click()
        self.assertTrue('employees/documents-overview/accountant' in self.driver.current_url)

    def dochazka(self):
        dochazka_page = DochazkaPage(self.driver)
        dochazka_page.planovani_smen_click()
        self.assertTrue('attendance/plan' in self.driver.current_url)
        dochazka_page()
        dochazka_page.rekapitulace_click()
        self.assertTrue('attendance/recapitulation' in self.driver.current_url)

    def zadosti(self):
        zadosti_page = ZadostiPage(self.driver)
        zadosti_page.zadosti_click()
        self.assertTrue('approval-requests/handled-by-me' in self.driver.current_url)

    def system(self):
        system_page = SystemPage(self.driver)
        system_page.uzivatele_click()
        self.assertTrue('users' in self.driver.current_url)
        system_page()
        system_page.role_click()
        self.assertTrue('roles' in self.driver.current_url)
        system_page()
        system_page.spolecnosti_click()
        self.assertTrue('companies' in self.driver.current_url)
        system_page()
        system_page.nastaveni_aplikace_click()
        self.assertTrue('system-settings' in self.driver.current_url)
>>>>>>> d5040dcc32f19ea6d6db31461314147378cb0c40

    def tearDown(self):
        self.driver.quit()