from configs.locators import MenuAttendancePageLocators
from utils.Waiter import Waiter


class MenuAttendancePage:
    def __init__(self, driver):
        self.driver = driver
        self.main = self.driver.find_element(*MenuAttendancePageLocators.main)
        self.link_plan = self.main.find_element(*MenuAttendancePageLocators.link_plan)
        self.link_recapitulation = self.main.find_element(*MenuAttendancePageLocators.link_recapitulation)
        self.waiter = Waiter(self.driver, MenuAttendancePageLocators.main)

    def __call__(self):
        self.__init__(self.driver)
        return self

    def click_link_plan(self):
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_plan.click()

    def click_link_recapitulation(self):
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_recapitulation.click()
