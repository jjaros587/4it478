from tests.BaseTest import BaseTest
from pages.employees.newEmployee.NewEmployeePage import NewEmployeePage
from ddt import ddt, file_data
from configs import config


@ddt
class NewEmployeeTest(BaseTest):
    path = config.dataPath + 'employees\\newEmployee\\'

    @file_data(path + "new_employee_positive.json")
    def test_add_employee_positive(self, employee):
        NewEmployeePage(self.driver).test_basic_positive(self.email, employee)

    @file_data(path + "new_employee_negative.json")
    def add_employee_mandatory_field_empty(self, employee, error):
        NewEmployeePage(self.driver).test_basic_negative(self.email, employee, error)
