from configs.locators import NewEmployeePageLoators
from utils.utilsTest import select_dropdown_item


class NewEmployeePage:
    def __init__(self, driver):
        self.driver = driver
        self.field_jmeno = self.driver.find_element(*NewEmployeePageLoators.field_jmeno)
        self.field_prijmeni = self.driver.find_element(*NewEmployeePageLoators.field_prijmeni)
        self.field_email = self.driver.find_element(*NewEmployeePageLoators.field_email)
        self.field_telefon = self.driver.find_element(*NewEmployeePageLoators.field_telefon)

        self.dropdown_schvalovatel = self.driver.find_element(*NewEmployeePageLoators.dropdown_schvalovatel)
        self.checkbox_souhlas = self.driver.find_element(*NewEmployeePageLoators.checkbox_souhlas)
        self.button_zalozit = self.driver.find_element(*NewEmployeePageLoators.button_zalozit)
        '''
        self.dropdown_jazyk = self.driver.find_element(*NewEmployeePageLoators.dropdown_jazyk)
        self.dropdown_útvar = self.driver.find_element(*NewEmployeePageLoators.dropdown_útvar)
        '''

    def __call__(self):
        self.__init__(self.driver)

    def fill_field_jmeno(self, jmeno):
        self.field_jmeno.clear()
        self.field_jmeno.send_keys(jmeno)

    def fill_field_prijmeni(self, prijmeni):
        self.field_prijmeni.clear()
        self.field_prijmeni.send_keys(prijmeni)

    def fill_field_email(self, email):
        self.field_email.clear()
        self.field_email.send_keys(email)

    def fill_field_telefon(self, telefon):
        self.field_telefon.clear()
        self.field_telefon.send_keys(telefon)

    def fill_dropdown_schvalovatel(self, schvalovatel):
        select_dropdown_item(self.driver, self.dropdown_schvalovatel, schvalovatel)

    def click_checkbox_souhlas(self):
        self.checkbox_souhlas.click()

    def click_button_zalozit(self):
        self.button_zalozit.click()

    def fill_form_basic_positive(self, email, jmeno, prijmeni, telefon, schvalovatel):
        self.fill_field_jmeno(jmeno)
        self.fill_field_prijmeni(prijmeni)
        self.fill_field_email(email)
        self.fill_field_telefon(telefon)
        self.fill_dropdown_schvalovatel(schvalovatel)
        self.click_checkbox_souhlas()
        self.click_button_zalozit()

