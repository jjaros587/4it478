from pages.employees.employments.cancelEmployment.CancelEmploymentPage import CancelEmploymentPage
from tests.BaseTest import BaseTest
from ddt import ddt, file_data
from configs import config


@ddt
class CancelEmploymentTest(BaseTest):
    path = config.dataPath + 'employees\\employments\\cancelEmployment\\'

    @file_data(path + "cancel_employment_positive.json")
    def test_cancel_employment_positive(self, employee, inquiry, employment, cancel_employment):
        CancelEmploymentPage(self.driver).test_basic_positive(self.email, employee, inquiry, employment, cancel_employment)
