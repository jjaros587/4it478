from tests.BaseTest import BaseTest
from pages.login.LoginPage import LoginPage
from configs import config
from ddt import ddt, file_data
from utils.utilsTest import element_exists
from selenium.webdriver.common.by import By


@ddt
class LogInTest(BaseTest):
    path = config.dataPath + "login/"

    @file_data(path + "login_positive.json")
    def test_login_positive(self, username, password):
        login_page = LoginPage(self.driver)
        login_page.login(username, password)
        self.assertTrue('users' in self.driver.current_url)

    @file_data(path + "login_negative.json")
    def test_login_negative(self, username, password):
        login_page = LoginPage(self.driver)
        login_page.login(username, password)
        self.assertFalse('users' in self.driver.current_url)
        self.assertTrue(element_exists(self.driver, (By.CLASS_NAME, 'alert')), "Chybová hláška se nezobrazila")

    def tearDown(self):
        self.driver.quit()