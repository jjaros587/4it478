from tests.BaseTest import BaseTest
from pages.employees.PrehledZamestnancuPage import PrehledZamestnancuPage
from pages.employees.personalInquiry.PersonalInquiry1Page import PersonalInquiry1Page
from pages.employees.personalInquiry.PersonalInquiry2Page import PersonalInquiry2Page
from pages.employees.personalInquiry.PersonalInquiry3Page import PersonalInquiry3Page
from pages.employees.employments.NewEmployment1Page import NewEmployment1Page
from pages.employees.employments.NewEmployment2Page import NewEmployment2Page
from pages.employees.employments.NewEmployment3Page import NewEmployment3Page
from pages.employees.employments.NewEmployment4Page import NewEmployment4Page
from pages.navigation.ZamestnanciPage import ZamestnanciPage
from pages.employees.NewEmployeePage import NewEmployeePage
from pages.employees.EmployeeCardPage import EmployeeCardPage
from pages.login.LoginPage import LoginPage
from ddt import ddt, file_data
from configs import config
from utils.utilsTest import get_login_credentials
from configs.locators import OtherLocators

@ddt
class NewEmploymentTest(BaseTest):
    path = config.dataPath + 'employeeManagement/employments/'

    @file_data(path + "employment_positive.json")
    def test_add_employment_positive(self, employee, inquiry1, inquiry2, inquiry3, company, employment):
        LoginPage(self.driver).login(*get_login_credentials())
        ZamestnanciPage(self.driver).prehled_zamestnancu_click()
        PrehledZamestnancuPage(self.driver).click_buttonNovyZamestnanec()
        NewEmployeePage(self.driver).fill_form_basic_positive(self.email, *employee.values())

        self.driver.find_element_by_link_text("kartu zaměstnance").click()
        self.driver.find_element_by_id("btn-fill-inquiry").click()

        self.assertTrue('contract-info' in self.driver.current_url)
        PersonalInquiry1Page(self.driver).fill_form_part1_positive(*inquiry1.values())
        self.assertTrue('other-employee-details' in self.driver.current_url)
        PersonalInquiry2Page(self.driver).fill_form_part2_positive(*inquiry2.values())
        self.assertTrue('wages-info' in self.driver.current_url)
        PersonalInquiry3Page(self.driver).fill_form_part3_positive(*inquiry3.values())

        self.assertTrue('summary' in self.driver.current_url)
        self.driver.find_element(*OtherLocators.button_schvalitDotaznik).click()
        self.assertTrue('employee-card' in self.driver.current_url)

        EmployeeCardPage(self.driver).pridat_pomer(company)

        NewEmployment1Page(self.driver).fill_form_part1_basic_positive(*employment['part1'].values())
        NewEmployment2Page(self.driver).fill_form_part2_basic_positive(*employment['part2'].values())
        NewEmployment3Page(self.driver).fill_form_part3_basic_positive()
        NewEmployment4Page(self.driver).fill_form_part4_basic_positive()

        self.driver.find_element(*OtherLocators.button_schvalitPomer).click()
        self.assertTrue('created' in self.driver.current_url)








