from pages.employees.newEmployee.NewEmployeePage import NewEmployeePage
from pages.employees.personalInquiry.PersonalInquiry1Page import PersonalInquiry1Page
from pages.employees.personalInquiry.PersonalInquiry2Page import PersonalInquiry2Page
from pages.employees.personalInquiry.PersonalInquiry3Page import PersonalInquiry3Page
from configs.locators import OtherLocators


class PersonalInquiryMasterPage:
    def __init__(self, driver):
        self.driver = driver

    def test_basic_positive(self, email, employee, inquiry):
        self.start(email, employee)
        PersonalInquiry1Page(self.driver).test_basic_positive(inquiry['part1'])
        PersonalInquiry2Page(self.driver).test_basic_positive(inquiry['part2'])
        PersonalInquiry3Page(self.driver).test_basic_positive(inquiry['part3'])
        self.driver.find_element(*OtherLocators.button_approve_or_send_to_approval).click()
        assert 'employee-card' in self.driver.current_url, "Nepodařilo se schválit osobní dotazník!"

    def test_negative_empty_fields_part1(self, email, employee, inquiry, error):
        self.start(email, employee)
        PersonalInquiry1Page(self.driver).test_negative_empty_fields(inquiry['part1'], error)

    def start(self, email, employee):
        NewEmployeePage(self.driver).test_basic_positive(email, employee)
        self.driver.find_element_by_link_text("kartu zaměstnance").click()
        self.driver.find_element_by_id("btn-fill-inquiry").click()
        assert 'contract-info' in self.driver.current_url, "Nedošlo k přesměrování na 1. část formuláře pro osobní dotazník!"



