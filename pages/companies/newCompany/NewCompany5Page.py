from configs.locators import NewCompany5PageLocators
from utils.utilsTest import click_radio_by_value
from utils.utilsTest import select_dropdown_item_known_value, select_dropdown_item
from utils.utilsTest import click_checkbox_by_value
import time


class NewCompany5Page:

    def __init__(self, driver):
        self.driver = driver
        self.link_add_work_position = self.driver.find_element(*NewCompany5PageLocators.link_add_work_position)
        self.button_finish = self.driver.find_element(*NewCompany5PageLocators.button_finish)

    def __call__(self):
        self.field_name = self.driver.find_element(*NewCompany5PageLocators.field_name)
        self.field_localized_names_for_contract_cs = self.driver.find_element(*NewCompany5PageLocators.field_localized_names_for_contract_cs)
        self.radio_managing_position = self.driver.find_elements(*NewCompany5PageLocators.radio_managing_position)
        self.dropdown_medical_examination_work_category = self.driver.find_element(*NewCompany5PageLocators.dropdown_medical_examination_work_category)
        self.dropdown_job_group = self.driver.find_element(*NewCompany5PageLocators.dropdown_job_group)
        self.dropdown_shift_type = self.driver.find_element(*NewCompany5PageLocators.dropdown_shift_type)
        self.radio_working_hours_type = self.driver.find_elements(*NewCompany5PageLocators.radio_working_hours_type)
        self.dropdown_default_trial_period_months = self.driver.find_element(*NewCompany5PageLocators.dropdown_default_trial_period_months)
        self.dropdown_job_classification_id = self.driver.find_element(*NewCompany5PageLocators.dropdown_job_classification_id)
        self.checkbox_roles = self.driver.find_elements(*NewCompany5PageLocators.checkbox_roles)

        self.button_save = self.driver.find_element(*NewCompany5PageLocators.button_save)
        self.link_close = self.driver.find_element(*NewCompany5PageLocators.link_close)

    def click_link_add_work_position(self):
        self.link_add_work_position.click()

    def fill_field_name(self, name):
        self.field_name.clear()
        self.field_name.send_keys(name)

    def fill_field_localized_names_for_contract_cs(self, localized_names_for_contract_cs):
        self.field_localized_names_for_contract_cs.click()
        self.field_localized_names_for_contract_cs.clear()
        self.field_localized_names_for_contract_cs.send_keys(localized_names_for_contract_cs)

    def select_radio_managing_position(self, managing_position):
        click_radio_by_value(self.radio_managing_position, managing_position)

    def fill_dropdown_medical_examination_work_category(self, medical_examination_work_category):
        select_dropdown_item(self.driver, self.dropdown_medical_examination_work_category, medical_examination_work_category)

    def fill_dropdown_job_group(self, job_group):
        select_dropdown_item(self.driver, self.dropdown_job_group, job_group)

    def fill_dropdown_shift_type(self, shift_type):
        select_dropdown_item(self.driver, self.dropdown_shift_type, shift_type)

    def select_radioworking_hours_type(self, working_hours_type):
        click_radio_by_value(self.radio_working_hours_type, working_hours_type)

    def fill_dropdown_default_trial_period_months(self, default_trial_period_months):
        select_dropdown_item(self.driver, self.dropdown_default_trial_period_months, default_trial_period_months)

    def fill_dropdown_job_classification_id(self, job_classification_id):
        select_dropdown_item_known_value(self.driver, self.dropdown_job_classification_id, job_classification_id)

    def click_checkbox_role(self, roles):
        for role in roles:
            click_checkbox_by_value(self.checkbox_roles, role)

    def fill_dropdown_superior_of_position_ids(self, superior_of_position_ids):
        element = self.driver.find_element(*NewCompany5PageLocators.dropdown_superior_of_position_ids)
        for position in superior_of_position_ids:
            select_dropdown_item(self.driver, element, position)

    def click_button_finish(self):
        self.button_finish.click()

    def click_button_save(self):
        self.button_save.click()

    def click_link_close(self):
        self.link_close.click()

    def fill_form(self, company5):
        for item in company5:
            self.click_link_add_work_position()
            self()
            try:
                self.fill_field_name(item['name'])
                self.fill_field_localized_names_for_contract_cs(item['localized_names_for_contract_cs'])
                self.select_radio_managing_position(item['managing_position'])
                self.fill_dropdown_medical_examination_work_category(item['medical_examination_work_category'])
                self.fill_dropdown_job_group(item['job_group'])
                self.fill_dropdown_shift_type(item['shift_type'])
                self.select_radioworking_hours_type(item['working_hours_type'])
                self.fill_dropdown_default_trial_period_months(item['default_trial_period_months'])
                self.fill_dropdown_job_classification_id(item['job_classification_id'])
                self.click_checkbox_role(item['roles'])
                if "ROLE_SUPERIOR" in item['roles']:
                    self.fill_dropdown_superior_of_position_ids(item['superior_of_position_ids'])
            except Exception as e:
                self.click_link_close()
                assert False, "Nepovedlo se přidat novou pozici: " + str(e)
            else:
                self.click_button_save()
            time.sleep(1)
        self.click_button_finish()

    def test_basic_positive(self, company5):
        self.fill_form(company5)
        assert 'setup-finished' in self.driver.current_url, "Nepovedlo se odeslat 5. část formuláře pro vytvoření nové společnosti!"
