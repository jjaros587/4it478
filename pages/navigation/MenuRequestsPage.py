from configs.locators import MenuRequestsPageLocators


class MenuRequestsPage:
    def __init__(self, driver):
        self.driver = driver
        self.main = self.driver.find_element(*MenuRequestsPageLocators.main)

    def __call__(self):
        self.__init__(self.driver)
        return self

    def click_approval_requests(self):
        self.main.click()
