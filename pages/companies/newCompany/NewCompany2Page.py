from configs.locators import NewCompany2PageLocators
from selenium.webdriver.common.keys import Keys
from utils.utilsTest import click_radio_by_value


class NewCompany2Page:
    def __init__(self, driver):
        self.driver = driver
        self.radio_personal_number_format = self.driver.find_elements(*NewCompany2PageLocators.radio_personal_number_format)
        self.radio_personal_number_editable = self.driver.find_elements(*NewCompany2PageLocators.radio_personal_number_editable)
        self.field_minimal_personal_number = self.driver.find_element(*NewCompany2PageLocators.field_minimal_personal_number)
        self.field_balancing_period = self.driver.find_element(*NewCompany2PageLocators.field_balancing_period)
        self.radio_balancing_period_units = self.driver.find_elements(*NewCompany2PageLocators.radio_balancing_period_units)
        self.field_balancing_period_starts_when = self.driver.find_element(*NewCompany2PageLocators.field_balancing_period_starts_when)
        self.button_save_and_continue = self.driver.find_element(*NewCompany2PageLocators.button_save_and_continue)

    def __call__(self):
        self.__init__(self.driver)

    def select_radio_personal_number_format(self, os_format):
        click_radio_by_value(self.radio_personal_number_format, os_format)

    def select_radio_personal_number_editable(self, os_editable):
        click_radio_by_value(self.radio_personal_number_editable, os_editable)

    def fill_field_minimal_personal_number(self, os_min):
        self.field_minimal_personal_number.clear()
        self.field_minimal_personal_number.send_keys(os_min)

    def fill_field_balancing_period(self, balancing_period):
        self.field_balancing_period.clear()
        self.field_balancing_period.send_keys(balancing_period)

    def select_radio_balancing_period_units(self, balancing_period_units):
        click_radio_by_value(self.radio_balancing_period_units, balancing_period_units)

    def fill_field_balancing_period_starts_when(self, balancing_period_starts_when):
        self.field_balancing_period_starts_when.clear()
        self.field_balancing_period_starts_when.send_keys(balancing_period_starts_when)
        self.field_balancing_period_starts_when.send_keys(Keys.ENTER)

    def click_button_save_and_continue(self):
        self.button_save_and_continue.click()

    def fill_form(self, company2):
        self.fill_field_balancing_period(company2['balancing_period'])
        self.select_radio_balancing_period_units(company2['balancing_period_units'])
        self.fill_field_balancing_period_starts_when(company2['balancing_period_starts_when'])
        self.click_button_save_and_continue()

    def test_basic_positive(self, company2):
        self.fill_form(company2)
        assert 'company-documents' in self.driver.current_url, \
            "Nepovedlo se odeslat 2. část formuláře pro vytvoření nové společnosti!"
