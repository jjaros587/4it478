from pages.employees.employments.changeEmployment.ChangeEmployment1Page import ChangeEmployment1Page
from pages.employees.employments.changeEmployment.ChangeEmployment2BenefitsPage import ChangeEmployment2BenefitsPage
from pages.employees.employments.changeEmployment.ChangeEmployment2OthersPage import ChangeEmployment2OthersPage
from pages.employees.employments.changeEmployment.ChangeEmployment3Page import ChangeEmployment3Page
from pages.employees.employments.newEmployment.NewEmploymentMasterPage import NewEmploymentMasterPage
from pages.employees.employments.BaseChangePage import BaseChangePage
from configs.locators import BaseChangePageLocators


class ChangeEmploymentMasterPage(BaseChangePage):
    def __init__(self, driver):
        super().__init__(driver)

    def fill_form(self, email, employee, inquiry, employment, change_employment):
        NewEmploymentMasterPage(self.driver).test_basic_positive(email, employee, inquiry, employment)

        self.go_change()
        changes = change_employment['part2'].keys()
        ChangeEmployment1Page(self.driver).test_basic_positive(change_employment['part1'], changes)

        # Ve změnách nejsou benefity
        if "benefits" not in changes:
            ChangeEmployment2OthersPage(self.driver, changes).test_basic_positive(change_employment['part2'])
        # Ve změnách jsou pouze benefity
        elif len(changes) == 1:
            ChangeEmployment2BenefitsPage(self.driver).test_basic_positive(change_employment['part2'])
        # Ve změnách jsou kombinace
        else:
            ChangeEmployment2OthersPage(self.driver, changes).test_basic_positive(change_employment['part2'])
            ChangeEmployment2BenefitsPage(self.driver).test_basic_positive(change_employment['part2'])

        ChangeEmployment3Page(self.driver).test_basic_positive(change_employment['part3'])

        self.driver.find_element(*BaseChangePageLocators.button_create_amendment).click()
        assert 'created' in self.driver.current_url, "Nepodařilo se schválit dodatek pracovního poměru!"



