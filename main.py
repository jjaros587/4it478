from utils import utilsMain

utilsMain.delete_folders()
result = utilsMain.parse_args()
utilsMain.get_env(result['parse'], result['args'])
utilsMain.run_tests()
utilsMain.zip_screenshots()