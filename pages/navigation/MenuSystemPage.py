from configs.locators import MenuSystemPageLocators
from utils.utilsTest import scroll_to_top
from utils.Waiter import Waiter


class MenuSystemPage:
    def __init__(self, driver):
        self.driver = driver
        self.main = self.driver.find_element(*MenuSystemPageLocators.main)
        self.link_users = self.main.find_element(*MenuSystemPageLocators.link_users)
        self.link_roles = self.main.find_element(*MenuSystemPageLocators.link_roles)
        self.link_companies = self.main.find_element(*MenuSystemPageLocators.link_companies)
        self.link_system_settings = self.main.find_element(*MenuSystemPageLocators.link_system_settings)
        self.link_licence = self.main.find_element(*MenuSystemPageLocators.link_licence)
        self.waiter = Waiter(self.driver, MenuSystemPageLocators.main)

    def __call__(self):
        self.__init__(self.driver)
        return self

    def click_link_users(self):
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_users.click()

    def click_link_roles(self):
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_roles.click()

    def click_link_companies(self):
        scroll_to_top(self.driver)
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_companies.click()

    def click_link_system_settings(self):
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_system_settings.click()

    def click_link_licence(self):
        self.main.click()
        self.waiter.wait_for_menu()
        self.link_licence.click()
