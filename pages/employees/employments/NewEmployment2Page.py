from configs.locators import NewEmployment2PageLocators
from utils.utilsTest import select_dropdown_item
from utils.utilsTest import click_radio_by_value


class NewEmployment2Page:
    def __init__(self, driver):
        self.driver = driver
        self.dropdown_trvani = self.driver.find_element(*NewEmployment2PageLocators.dropdown_trvani)
        self.dropdown_zkusebniDoba = self.driver.find_element(*NewEmployment2PageLocators.dropdown_zkusebniDoba)
        self.dropdown_smennyProvoz = self.driver.find_element(*NewEmployment2PageLocators.dropdown_smennyProvoz)
        self.field_uvazek = self.driver.find_element(*NewEmployment2PageLocators.field_uvazek)
        self.radio_pracovniDoba = self.driver.find_elements(*NewEmployment2PageLocators.radio_pracovniDoba)
        self.field_mzda = self.driver.find_element(*NewEmployment2PageLocators.field_mzda)
        self.button_pokracovat = self.driver.find_element(*NewEmployment2PageLocators.button_pokracovat)

    def __call__(self):
        self.__init__(self.driver)

    def fill_dropdown_trvani(self, trvani):
        select_dropdown_item(self.driver, self.dropdown_trvani, trvani)

    def fill_dropdown_zkusebniDoba(self, zkusebniDoba):
        select_dropdown_item(self.driver, self.dropdown_zkusebniDoba, zkusebniDoba)

    def fill_dropdown_smennyProvoz(self, smennyProvoz):
        select_dropdown_item(self.driver, self.dropdown_smennyProvoz, smennyProvoz)

    def fill_field_uvazek(self, uvazek):
        self.field_uvazek.clear()
        self.field_uvazek.send_keys(uvazek)

    def select_radio_pracovni_doba(self, pracovniDoba):
        click_radio_by_value(self.radio_pracovniDoba, pracovniDoba)

    def fill_field_mzda(self, zakladniMzda):
        self.field_mzda.clear()
        self.field_mzda.send_keys(zakladniMzda)

    def click_button_pokracovat(self):
        self.button_pokracovat.click()

    def fill_form_part2_basic_positive(self, trvani, zkusebniDoba, smennyProvoz, uvazek, pracovniDoba, zakladniMzda):
        self.fill_dropdown_trvani(trvani)
        self.fill_dropdown_zkusebniDoba(zkusebniDoba)
        self.fill_dropdown_smennyProvoz(smennyProvoz)
        self.fill_field_uvazek(uvazek)
        self.select_radio_pracovni_doba(pracovniDoba)
        self.fill_field_mzda(zakladniMzda)
        self.click_button_pokracovat()

