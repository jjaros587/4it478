from configs.locators import ZamestnanciPageLocators
from selenium.webdriver.support.ui import WebDriverWait
from utils.AttributeHasValue import AttributeHasValue
from utils.utilsTest import waiter


class ZamestnanciPage:
    def __init__(self, driver):
        self.driver = driver
        self.zamestnanci = self.driver.find_element(*ZamestnanciPageLocators.main)
        self.prehledZamestnancu = self.zamestnanci.find_element(*ZamestnanciPageLocators.prehledZamestnancu)
        self.nastaveniDokumentu = self.zamestnanci.find_element(*ZamestnanciPageLocators.nastaveniDokumentu)
        self.prehledZmen = self.zamestnanci.find_element(*ZamestnanciPageLocators.prehledZmen)
        self.nahraneDokumenty = self.zamestnanci.find_element(*ZamestnanciPageLocators.nahraneDokumenty)

    def __call__(self):
        self.__init__(self.driver)

    def prehled_zamestnancu_click(self):
        self.zamestnanci.click()
        waiter(self.driver, ZamestnanciPageLocators.main, 2000, 'class', 'is-open')
        self.prehledZamestnancu.click()

    def nastaveni_dokumentu_click(self):
        self.zamestnanci.click()
        waiter(self.driver, ZamestnanciPageLocators.main, 2000, 'class', 'is-open')
        self.nastaveniDokumentu.click()

    def prehled_zmen_click(self):
        self.zamestnanci.click()
        waiter(self.driver, ZamestnanciPageLocators.main, 2000, 'class', 'is-open')
        self.prehledZmen.click()

    def nahrane_dokumenty_click(self):
        self.zamestnanci.click()
        waiter(self.driver, ZamestnanciPageLocators.main, 2000, 'class', 'is-open')
        self.nahraneDokumenty.click()
