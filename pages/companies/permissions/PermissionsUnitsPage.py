from pages.companies.newCompany.NewCompanyMasterPage import NewCompanyMasterPage
from pages.employees.employments.newEmployment.NewEmploymentMasterPage import NewEmploymentMasterPage
from pages.SearchPage import SearchPage


class PermissionsUnitsPage:

    def __init__(self, driver):
        self.driver = driver

    def __call__(self):
        self.__init__(self.driver)
        return self

    def create_data(self, company, email1, email2, employee1, employee2):
        NewCompanyMasterPage(self.driver).test_basic_positive(False, company)
        NewEmploymentMasterPage(self.driver).test_basic_positive(email1, employee1['employee'], employee1['inquiry'], employee1['employment'])
        NewEmploymentMasterPage(self.driver).test_basic_positive(email2, employee2['employee'], employee2['inquiry'], employee2['employment'])

    def test_user(self, email, test_rc):
        SearchPage(self.driver).find_user_basic(test_rc)
        assert "employee-card" not in self.driver.current_url, "Zaměstnanec " + email + " by neměl vidět zaměstnance " + test_rc
