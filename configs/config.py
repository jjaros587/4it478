import os
import sys


environments = {
    'dev': 'http://tria.epk-tech.com/',
    'test': '',
    'stage': 'https://stage.decasport.triahr.com/'
}

browsers = [
    {"allowed": True, "name": "chrome"},
    {"allowed": False, "name": "firefox"}
]

dataPath = os.path.dirname(os.path.realpath(sys.modules['__main__'].__file__)) + "\\data\\"
