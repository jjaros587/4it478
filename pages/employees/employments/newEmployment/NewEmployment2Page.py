from configs.locators import NewEmployment2PageLocators
from utils.utilsTest import select_dropdown_item
from utils.utilsTest import click_radio_by_value


class NewEmployment2Page:
    def __init__(self, driver):
        self.driver = driver
        self.dropdown_duration_months = self.driver.find_element(*NewEmployment2PageLocators.dropdown_duration_months)
        self.dropdown_trial_period_months = self.driver.find_element(*NewEmployment2PageLocators.dropdown_trial_period_months)
        self.dropdown_shift_type = self.driver.find_element(*NewEmployment2PageLocators.dropdown_shift_type)
        self.field_working_hours_per_week = self.driver.find_element(*NewEmployment2PageLocators.field_working_hours_per_week)
        self.radio_working_hours_type = self.driver.find_elements(*NewEmployment2PageLocators.radio_working_hours_type)
        self.field_basic_salary = self.driver.find_element(*NewEmployment2PageLocators.field_basic_salary)
        self.button_save_and_continue = self.driver.find_element(*NewEmployment2PageLocators.button_save_and_continue)

    def __call__(self):
        self.__init__(self.driver)

    def fill_dropdown_duration_months(self, duration_months):
        select_dropdown_item(self.driver, self.dropdown_duration_months, duration_months)

    def fill_dropdown_trial_period_months(self, trial_period_months):
        select_dropdown_item(self.driver, self.dropdown_trial_period_months, trial_period_months)

    def fill_dropdown_shift_type(self, shift_type):
        select_dropdown_item(self.driver, self.dropdown_shift_type, shift_type)

    def fill_field_working_hours_per_week(self, working_hours_per_week):
        self.field_working_hours_per_week.clear()
        self.field_working_hours_per_week.send_keys(working_hours_per_week)

    def select_radio_working_hours_type(self, working_hours_type):
        click_radio_by_value(self.radio_working_hours_type, working_hours_type)

    def fill_field_basic_salary(self, basic_salary):
        self.field_basic_salary.clear()
        self.field_basic_salary.send_keys(basic_salary)

    def click_button_save_and_continue(self):
        self.button_save_and_continue.click()

    def fill_form(self, employment2):
        self.fill_dropdown_duration_months(employment2['duration_months'])
        self.fill_dropdown_trial_period_months(employment2['trial_period_months'])
        self.fill_dropdown_shift_type(employment2['shift_type'])
        self.fill_field_working_hours_per_week(employment2['working_hours_per_week'])
        self.select_radio_working_hours_type(employment2['working_hours_type'])
        self.fill_field_basic_salary(employment2['basic_salary'])
        self.click_button_save_and_continue()

    def test_basic_positive(self, employment2):
        self.fill_form(employment2)
