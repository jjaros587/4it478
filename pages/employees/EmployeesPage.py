from configs.locators import EmployeesPageLocators


class EmployeesPage:
    def __init__(self, driver):
        self.driver = driver
        self.button_new_employee = self.driver.find_element(*EmployeesPageLocators.button_new_employee)

    def __call__(self):
        self.__init__(self.driver)

    def click_button_new_employee(self):
        self.button_new_employee.click()
        assert 'new-employee' in self.driver.current_url, "Nedošlo k přesměrování na formulář pro vytvoření nového zaměstnance!"