from tests.BaseTest import BaseTest
from pages.employees.employments.changeEmployment.ChangeEmploymentMasterPage import ChangeEmploymentMasterPage
from ddt import ddt, file_data
from configs import config


@ddt
class ChangeEmploymentTest(BaseTest):
    path = config.dataPath + 'employees\\employments\\changeEmployment\\'

    @file_data(path + "change_employment_positive.json")
    def test_change_employment_positive(self, employee, inquiry, employment, change_employment):
        ChangeEmploymentMasterPage(self.driver).fill_form(self.email, employee, inquiry, employment, change_employment)
