from configs.locators import DochazkaPageLocators
from selenium.webdriver.support.ui import WebDriverWait
from utils.AttributeHasValue import AttributeHasValue
from utils.utilsTest import waiter


class DochazkaPage:
    def __init__(self, driver):
        self.driver = driver
        self.dochazka = self.driver.find_element(*DochazkaPageLocators.main)
        self.planovaniSmen = self.dochazka.find_element(*DochazkaPageLocators.planovaniSmen)
        self.rekapitulace = self.dochazka.find_element(*DochazkaPageLocators.rekapitulace)

    def __call__(self):
        self.__init__(self.driver)

    def planovani_smen_click(self):
        self.dochazka.click()
        waiter(self.driver, DochazkaPageLocators.main, 2000, 'class', 'is-open')
        self.planovaniSmen.click()

    def rekapitulace_click(self):
        self.dochazka.click()
        waiter(self.driver, DochazkaPageLocators.main, 2000, 'class', 'is-open')
        self.rekapitulace.click()
