from configs.locators import SearchPageLocators
import time


class SearchPage:
    def __init__(self, driver):
        self.driver = driver
        self.button_show_search_form = self.driver.find_element(*SearchPageLocators.button_show_search_form)
        self.field_search = self.driver.find_element(*SearchPageLocators.field_search)
        self.button_search = self.driver.find_element(*SearchPageLocators.button_search)

    def __call__(self):
        self.radio_scope_users = self.driver.find_element(*SearchPageLocators.radio_scope_users)

    def find_user(self, value):
        self.__call__()
        self.button_show_search_form.click()
        self.radio_scope_users.click()
        time.sleep(0.1)
        self.field_search.send_keys(value)
        self.button_search.click()

    def find_user_basic(self, rc):
        self.button_show_search_form.click()
        self.field_search.send_keys(rc)
        self.button_search.click()

    def delete_user(self, email):
        delimiter = 'user/'
        try:
            self.find_user(email)
            link = self.driver.current_url
            if delimiter in link:
                part = link.split(delimiter)
                link = part[0] + "delete-" + delimiter + part[1]
                self.driver.get(link)
        except Exception as e:
            return False
        else:
            return True
