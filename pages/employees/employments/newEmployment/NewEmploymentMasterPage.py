from pages.employees.employments.newEmployment.NewEmployment1Page import NewEmployment1Page
from pages.employees.employments.newEmployment.NewEmployment2Page import NewEmployment2Page
from pages.employees.employments.newEmployment.NewEmployment3Page import NewEmployment3Page
from pages.employees.employments.newEmployment.NewEmployment4Page import NewEmployment4Page
from pages.employees.EmployeeCardPage import EmployeeCardPage
from pages.employees.personalInquiry.PersonalInquiryMasterPage import PersonalInquiryMasterPage
from pages.companies.newCompany.NewCompanyMasterPage import NewCompanyMasterPage
from configs.locators import OtherLocators


class NewEmploymentMasterPage:
    def __init__(self, driver):
        self.driver = driver

    def test_basic_positive(self, company, email, employee, inquiry, employment):
        NewCompanyMasterPage(self.driver).test_basic_positive(False, company)
        self.add_employment_without_company(email, employee, inquiry, employment)

    def add_employment_without_company(self, email, employee, inquiry, employment):
        PersonalInquiryMasterPage(self.driver).test_basic_positive(email, employee, inquiry)
        EmployeeCardPage(self.driver).add_employment(employment['company'])
        NewEmployment1Page(self.driver).fill_form(employment['part1'])
        NewEmployment2Page(self.driver).fill_form(employment['part2'])
        NewEmployment3Page(self.driver).fill_form(employment['part3'])
        NewEmployment4Page(self.driver).fill_form(employment['part4'])
        self.driver.find_element(*OtherLocators.button_create_employment).click()
        assert 'created' in self.driver.current_url, "Nepovedlo se schválit nový poměr!"



