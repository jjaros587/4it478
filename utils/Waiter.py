from selenium.common.exceptions import TimeoutException
import time


class Waiter:
    def __init__(self, driver, locator, attribute=None, value=None, seconds=2, step=0.1):
        self.driver = driver
        self.locator = locator
        self.attribute = attribute
        self.value = value
        self.seconds = seconds
        self.step = step

    def __call__(self, attribute, value):
        self.attribute = attribute
        self.value = value

    def wait(self):
        while self.seconds > 0:
            if self.value in self.driver.find_element(*self.locator).get_attribute(self.attribute):
                return True
            time.sleep(self.step)
            self.seconds -= self.step
        raise TimeoutException

    def wait_for_menu(self):
        self.__call__("class", "is-open")
        self.wait()
