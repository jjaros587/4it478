from tests.BaseTest import BaseTest
from pages.login.LoginPage import LoginPage
from pages.SearchPage import SearchPage
from ddt import ddt, file_data
from configs import config
from utils.utilsTest import get_login_credentials
import os

@ddt
class DeleteUsers(BaseTest):
    path = config.dataPath

    @classmethod
    def setUpClass(cls):
        super(DeleteUsers, cls).setUp(cls)
        login_page = LoginPage(cls.driver)
        login_page.login(*get_login_credentials())

    def setUp(self):
        self.driver.get(config.url[os.environ.get('env')])

    @file_data(path + 'delete.json')
    def test_delete_users(self, value):
        self.driver.get(config.url[os.environ.get('env')])
        search_page = SearchPage(self.driver)
        if not search_page.delete_user(value):
            self.fail("Uživatele s emailem "+value+" se nepodařilo smazat!")

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        super(DeleteUsers, cls).tearDown(cls)


