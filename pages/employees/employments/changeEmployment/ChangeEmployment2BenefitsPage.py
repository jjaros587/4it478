from configs.locators import ChangeEmployment2PageLocators
from utils.utilsTest import select_dropdown_item
import time


class ChangeEmployment2BenefitsPage:
    def __init__(self, driver):
        self.driver = driver
        self.button_add_benefit = self.driver.find_element(*ChangeEmployment2PageLocators.button_add_benefit)
        self.button_save_and_continue = self.driver.find_element(*ChangeEmployment2PageLocators.button_save_and_continue)

    def __call__(self):
        self.dropdown_type_id = self.driver.find_element(*ChangeEmployment2PageLocators.dropdown_type_id)
        self.button_save = self.driver.find_element(*ChangeEmployment2PageLocators.button_save)
        self.link_close = self.driver.find_element(*ChangeEmployment2PageLocators.link_close)

    def click_button_add_benefit(self):
        self.button_add_benefit.click()

    def fill_dropdown_type_id(self, type_id):
        select_dropdown_item(self.driver, self.dropdown_type_id, type_id)

    def click_button_save(self):
        self.button_save.click()

    def click_link_close(self):
        self.link_close.click()

    def click_button_save_and_continue(self):
        time.sleep(0.5)
        self.button_save_and_continue.click()

    def fill_form(self, change_employment2):
        self.click_button_add_benefit()
        self()
        try:
            self.fill_dropdown_type_id(change_employment2['benefits']['type_id'])
        except Exception as e:
            self.click_link_close()
            assert False, "Nepovedlo se přidat benefit! " + str(e)
        else:
            self.click_button_save()
        self.click_button_save_and_continue()

    def test_basic_positive(self, change_employment2):
        self.fill_form(change_employment2)
        assert 'documents-and-evidence' in self.driver.current_url, "Nepodařilo se odeslat 2. část formuláře pro vytvoření dodatku!"

